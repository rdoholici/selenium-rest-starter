package starter.stepdefinitions.cucumbersteps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.stepdefinitions.serenitysteps.SerenitySeleniumSteps;


public class CommonSeleniumCucumberSteps {

    @Steps
    SerenitySeleniumSteps serenitySeleniumSteps;

   @Given("^the user navigates to the URL: (.*)$")
    public void navigateToUrl(String url) {
        serenitySeleniumSteps.navigate(url);
   }

   @When("^the user inputs the text: \"(.*)\" in the (.*)$")
    public void inputTextInField(String text, String textField) {
       serenitySeleniumSteps.inputTextInField(text,textField);
   }

   @And("^the user presses the (.*)$")
    public void pressElement(String element){
       serenitySeleniumSteps.pressElement(element);
   }

   @And("^each result contains the word: \"(.*)\"$")
    public void verifyResults(String expectedString) {
       serenitySeleniumSteps.verifyResults(expectedString);
   }
}
