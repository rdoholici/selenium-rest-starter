package starter.stepdefinitions.cucumbersteps;

import io.cucumber.datatable.dependency.com.fasterxml.jackson.databind.util.JSONPObject;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.*;
import org.junit.Assert;

import java.util.List;
import java.util.Map;


public class CommonRestCucumberSteps {

    RequestSpecification rq;
    Response response;

    @Given("^the following parameters:$")
    public void prepareRequestSpecification(List<Map<String, String>> params) {
        rq = RestAssured.given();
        JSONObject bodyParams = new JSONObject ();
        for (Map<String, String> paramPair : params) {
            switch (paramPair.get("paramType")) {
                case "pathParam":
                    rq.pathParam(paramPair.get("paramKey"),paramPair.get("paramValue"));
                    break;
                case "header":
                    rq.header(paramPair.get("paramKey"),paramPair.get("paramValue"));
                    break;
                case "body":
                    bodyParams.put(paramPair.get("paramKey"),paramPair.get("paramValue"));
                    break;
            }
            rq.body(bodyParams.toJSONString());
        }
    }

    @When("^I perform a (.*) request to the URL: (.*) and path: (.*)$")
    public void performRestCall(String callType, String url, String path) {
        rq.baseUri(url);
        rq.basePath(path);
        switch (callType) {
            case "GET":
                performGetCall(rq);
                break;
            case "POST":
                performPostCall(rq);
                break;
        }
    }

    @Then("^the response is (.*)$")
    public void verifyResponseCode(String expectedCode) {
        String actualCode = String.valueOf(response.getStatusCode());
        Assert.assertTrue("The response expected is: " + expectedCode + " but the actual response is: " + actualCode,
                actualCode.equals(expectedCode));
    }

    private void performGetCall(RequestSpecification rq) {
        response = rq.when().log().all().get().prettyPeek();
    }

    private void performPostCall(RequestSpecification rq) {
        response = rq.when().log().all().post().prettyPeek();
    }
}
