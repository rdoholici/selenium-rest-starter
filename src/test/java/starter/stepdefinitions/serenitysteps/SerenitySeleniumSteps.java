package starter.stepdefinitions.serenitysteps;

import io.cucumber.java.Before;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import org.openqa.selenium.By;
import starter.CommonPage;

import java.util.HashMap;

public class SerenitySeleniumSteps {
    HashMap<String, String> xpathMap = new HashMap<String, String>();
    CommonPage commonPage;

    {
        xpathMap.put("googleSearchField", "//input[@title='Search']");
        xpathMap.put("googleResults","//div[@id='search']/div/div/div");
        xpathMap.put("googleSearchButton","//div[@class][not(@jsname)]/center/input[@value='Google Search']");
        xpathMap.put("googleImage","//img[@id='hplogo']");
    }

    @Step
    public void navigate(String url) {
        commonPage.getDriver().navigate().to(url);
    }

    @Step
    public void inputTextInField(String text, String textField) {
        commonPage.getDriver().findElement(By.xpath(xpathMap.get(textField))).sendKeys(text);
    }

    @Step
    public void pressElement(String button) {
        commonPage.getDriver().findElement(By.xpath(xpathMap.get(button))).click();
    }

    @Step
    public void verifyResults(String expectedString) {
        Assert.assertTrue("The text: " + expectedString + " is not present in all the results",
            commonPage.verifyResults(expectedString, xpathMap.get("googleResults")));
    }
}
