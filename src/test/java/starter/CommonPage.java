package starter;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class CommonPage extends PageObject {

    public boolean verifyResults(String expectedString, String xpath) {
        List<WebElement> results = getDriver().findElements(By.xpath(xpath));
        for (WebElement element : results) {
            if (!element.getText().toLowerCase().contains(expectedString.toLowerCase())) {
                return false;
            }
        }
        return true;
    }
}
