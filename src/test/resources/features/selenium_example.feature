Feature: Selenium example

  @TEST_ID=SeleniumExample
  Scenario: Perform google search
    Given the user navigates to the URL: https://www.google.com/en
    When the user inputs the text: "selenium" in the googleSearchField
    And the user presses the googleImage
    And the user presses the googleSearchButton
    Then each result contains the word: "selenium"
