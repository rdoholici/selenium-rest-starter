Feature: Search by keyword

  Scenario: Searching for a term
  Given I do a rest call
    Given Sergey is on the DuckDuckGo home page
    When he searches for "Cucumber"
    Then all the result titles should contain the word "Cucumber"
