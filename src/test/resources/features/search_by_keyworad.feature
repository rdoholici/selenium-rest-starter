Feature: REST call example feature

  @TEST_ID=RestExample
  Scenario: Perform REST call
    Given the following parameters:
#      | paramType | paramKey | paramValue |
#      | pathParam | pathkey1 | pathvalue1 |
#      | pathParam | pathkey2 | pathvalue2 |
#      | header    | headkey1 | headvalue1 |
#      | header    | headkey2 | headvalue2 |
#      | body      | bodkey1  | bodvalue1  |
#      | body      | bodkey2  | bodvalue2  |
    |||
    When I perform a GET request to the URL: http://dummy.restapiexample.com and path: /api/v1/employees
    Then the response is 200
